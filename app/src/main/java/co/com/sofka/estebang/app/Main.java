package co.com.sofka.estebang.app;

import co.com.sofka.estebang.domain.Persona;
import co.com.sofka.estebang.service.Service;

public class Main {

    public static void main(String[] args) {
        Service service = new Service();

        //Obtener una persona existente
        Persona personaExistente = service.obtenerPersona("111");
        System.out.println("Persona Existente: " + personaExistente.getNombre());

        //Obtener una persona no existente
        Persona personaNoExistente = service.obtenerPersona("555");
        System.out.println("Persona No Existente: " + personaNoExistente.getNombre());
    }
}
