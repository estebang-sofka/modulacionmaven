package co.com.sofka.estebang.usecase;

import co.com.sofka.estebang.domain.Persona;

import java.util.AbstractMap;
import java.util.Date;
import java.util.HashMap;

public class PersonaUseCase {

    private HashMap<String, Persona> map = new HashMap();

    public PersonaUseCase() {
        Persona persona1 = new Persona("111", "Esteban");
        Persona persona2 = new Persona("222", "Ramiro");
        Persona persona3 = new Persona("333", "Jairo");
        map.put(persona1.getId(), persona1);
        map.put(persona2.getId(), persona2);
        map.put(persona3.getId(), persona3);
    }

    public Persona obtenerPersona(String id) throws RuntimeException{
        if (map.containsKey(id))
            return map.get(id);
        throw new RuntimeException();
    }
}
